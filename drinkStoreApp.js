//Declaring and storing DOM-elements
const drinksElement = document.getElementById("drinks");
const priceElement = document.getElementById("price");
const addElement = document.getElementById("addToCart");
const cartElement = document.getElementById("cart");
const quantityElement = document.getElementById("quantity");
const payButtonElement = document.getElementById("pay");
const totalDueElement = document.getElementById("totalDue");

//State of what is needed in the application
//Representation of data, will be loaded into DOM-element eventually
let drinks = [];
let cart = [];
let totalDue = 0.0;


//Fetch the drinks from API
fetch("https://noroff-accelerate-drinks.herokuapp.com/drinks") // return a promise
    .then(response => response.json()) //chain the fetch with then, returns some of the data from the API fetched in line above
    .then(data => drinks = data) // Assign the drinks value to the data
    .then(drinks => addDrinksToMenu(drinks));  //call drinks to menu

// Store function in a varible
const addDrinksToMenu = (drinks) => {
    // For each drink in drinks, and all drink to optoin menu
    drinks.forEach(drink => addDrinkToMenu(drink));
    priceElement.innerText = drinks[0].price;
}

// Add inividual drink to the function aaDrinksToMenu
const addDrinkToMenu = (drink) => {
    // Create HTML-element that will print on site
    const drinkElement = document.createElement("option");

    // Add infomation to the optionHTML-elment
    drinkElement.value = drink.id;  //Id is uniqe
    drinkElement.appendChild(document.createTextNode(drink.description)); // Append child elment to HTML element, adding discription to Drink
    // Add option to the DOM
    drinksElement.appendChild(drinkElement); 
}


//Function
const handleDrinkMenuChange = e => {

    //Returns choocen drink
    const selectedDrink = drinks[e.target.selectedIndex];
    priceElement.innerText = selectedDrink.price;
}

const handleAddDrink = () => {
    const selectedDrink = drinks[drinksElement.selectedIndex];
    const quantity = parseInt(quantityElement.value); //Make sure it is int with paresInt

    const cartItem = document.createElement("li");
    const lineTotal = quantity * selectedDrink.price;

    cartItem.innerText = `${selectedDrink.description} ${selectedDrink.price} ${quantity} ${lineTotal.toFixed(2)}`;
    cartElement.appendChild(cartItem);
    
    totalDue += lineTotal;
    totalDueElement.innerText = `Total Due: ${totalDue.toFixed(2)}`;

}

const handlePay = () => {
    const totalPaid = prompt("Please enter the amount of money you wish to pay: ");
    const change = parseFloat(totalPaid) - totalDue;

    alert(`Totale change due: ${change.toFixed(2)}`)
}

//Eventlistner, first =  type, second = function
drinksElement.addEventListener("change", handleDrinkMenuChange);
addElement.addEventListener("click", handleAddDrink);
payButtonElement.addEventListener("click", handlePay);




